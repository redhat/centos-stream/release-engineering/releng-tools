#!/usr/bin/env groovy

def failure_email_sender = "Compose Alert <compose-alert@centos.org>"
def failure_email_recipient = "centos-stream-releng@redhat.com"

def composeattrs = null
def buildstatus = 'UNKNOWN'

pipeline{
    agent {label "baremetal"}

    options{
        timeout(time: 360, unit: 'MINUTES')
    }

    environment {
        KEYTAB = credentials('releng-ops-centos-stream-keytab')
    }

    stages {

        stage('Setup') {
            steps {
                deleteDir()
                checkout scm

                sh('kinit -k -t $KEYTAB -p "centos-stream@IPA.REDHAT.COM"')
            }
        }

        stage('Submit Stream 9 Compose') {
            steps {
                sh """
                scripts/compose-create.py -g c9s -t production -l RC-0.1
                """
                script {
                    composeattrs = readJSON file: 'response.json'
                    composeid = composeattrs['id']
                }

                sh "curl --negotiate -u : -o response.json https://odcs.stream.rdu2.redhat.com/api/1/composes/$composeid"
            }
        }

        stage('Wait for Compose to finish') {
            steps {
                timeout(time: 120, unit: 'MINUTES') { //120 minutes
                    script {
                        buildstatus = 'UNKNOWN'
                        Exception caughtException = null

                        // if an error occurs, abort this stage without failing the build
                        catchError(buildResult: null, stageResult: 'ABORTED') {
                            try {
                                while( true ) {
                                    sleep 60 // seconds
                                    sh "curl --negotiate -u : -o response.json https://odcs.stream.rdu2.redhat.com/api/1/composes/$composeid"

                                    composeattrs = readJSON file: 'response.json'

                                    if (composeattrs['state'] == 2) { //done
                                        buildstatus = 'SUCCESS'
                                        return
                                    } else if (composeattrs['state'] == 4) { //failed
                                        buildstatus = 'FAIL'
                                        return
                                    }
                                }
                            } catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException e) {
                                buildstatus = 'TIMEOUT'
                                error "Caught ${e.toString()}"
                            } catch (Throwable e) {
                                caughtException = e
                            }
                        } // catchError()

                        // if an unexpected exception occurred, go ahead and fail the build
                        if (caughtException) {
                            error caughtException.message
                        }
                    }
                } // timeout()
            }
        } // stage()

        stage('Report Compose Result') {
            steps {
                script {
                    def buildname = "unknown-build"
                    if (composeattrs) {
                        buildname = composeattrs['pungi_compose_id']
                        currentBuild.displayName = "$buildname"
                    }

                    echo "Build $buildname status: $buildstatus"

                    if (buildstatus != 'SUCCESS') {
                        def failure_subject = "Production compose $buildname status: $buildstatus!"
                        def failure_message = """Greetings.

Jenkins production compose build $buildname status is $buildstatus.

Job URL: ${BUILD_URL}"""
                        emailext to: failure_email_recipient,
                            from: failure_email_sender,
                            subject: failure_subject,
                            body: failure_message

                        error "Compose Status $buildstatus"
                    }
                    else {
                        sh """
                            sudo -u compose compose-create-legacy-composeinfo /mnt/centos//composes/production/$buildname/ --addon=BaseOS:AppStream --addon=BaseOS:CRB --addon=BaseOS:HighAvailability --addon=BaseOS:NFV --addon=BaseOS:RT --addon=BaseOS:ResilientStorage --addon=BaseOS:SAP --addon=BaseOS:SAPHANA
                        """
                    }
                }
            }
        }
    }


    post {
        always {
            archiveArtifacts allowEmptyArchive: true, artifacts: 'compose.txt,response.json', fingerprint: true
        }
    }
}
