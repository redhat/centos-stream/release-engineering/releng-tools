#!/usr/bin/env groovy

pipeline{
    agent none
    options{
        timeout(time: 16, unit: 'HOURS')
    }

    stages {
        stage('Stage Compose') {
            agent {label "baremetal"}
            steps {
                build job: 'stream9-compose-staging', wait: true, propagate: true, parameters: [[$class: 'StringParameterValue', name: 'dry_run', value: '']]
            }
        }

        stage('Sign Compose') {
            agent {label "baremetal"}
            steps {
                build job: 'stream9-compose-signing', wait: true, propagate: true
            }
        }

        stage('Sync Compose To Mirrors') {
            agent {label "baremetal"}
            steps {
                build job: 'stream9-compose-sync', wait: true, propagate: true
            }
        }

        stage('Wait for mirrors to propagate') {
            agent none
            steps {
                sleep(time: 10, unit: 'HOURS')
            }
        }

        stage('Sync Images to Cloud') {
            agent {label "baremetal"}
            steps {
                build job: 'stream9-production-images-release', wait: false, propagate: true
            }
        }
    }
}
